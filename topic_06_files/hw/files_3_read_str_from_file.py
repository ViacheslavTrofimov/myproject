"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""


def read_str_from_file(path):
    with open(path, 'r') as f:
        for line in f:
            print(line.strip())


if __name__ == '__main__':
    read_str_from_file('my_text.txt')


# def save_dict_to_file_classic(path, my_text):
#     with open(path, 'w') as my_file:
#         my_file.write(str(my_text))
#
#
# if __name__ == '__main__':
#     my_path = 'my_text.txt'
#     save_dict_to_file_classic(my_path, 'Tyger! Tyger! burning bright '
#                                        'In the forests of the night,'
#                                        'What immortal hand or eye '
#                                        'Could frame thy fearful symmetry?')
#
#     with open(my_path, 'r') as f:
#         my_dict_str = f.read()
#         print(my_dict_str)
