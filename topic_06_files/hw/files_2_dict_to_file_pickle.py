"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle


def save_dict_to_file_pickle(my_path, my_dict):
    with open(my_path, 'wb') as pickle_file:
        pickle.dump(my_dict, pickle_file)


if __name__ == '__main__':
    dictionary = {1: 2309, 2: 'trololo', 3: 678.87, 4: [5, 7, 'lol']}
    path = 'dict.pkl'

    save_dict_to_file_pickle(path, dictionary)

    with open(path, 'rb') as f:
        dict_loaded = pickle.load(f)

    print(f'type: {type(dict_loaded)}')
    print(f'equal: {dictionary == dict_loaded}')