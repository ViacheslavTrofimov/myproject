"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(path, my_dict):
    with open(path, 'w') as my_file:
        my_file.write(str(my_dict))


if __name__ == '__main__':
    my_path = 'my_dict.txt'
    save_dict_to_file_classic(my_path, {1: "one", 2: "two", 3: "three"})

    with open(my_path, 'r') as f:
        my_dict_str = f.read()
        print(my_dict_str)