"""
lambda перемножает аргументы a, b и c и выводит результат
"""

s = lambda a, b, c: print(a * b * c)

if __name__ == '__main__':
    s(44, 55, 66)
