"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(flower='Ромашка', colour='Белый', price=10.25):
    if isinstance(flower, str) and type(colour) == str and 0 < price < 10000:
        print(f"Цветок: {flower} | Цвет: {colour} | Цена: {price}")
    else:
        print('Flower and colour must be str and price must be more than 0 but less than 10000')


if __name__ == '__main__':
    (flower_with_default_vals(flower='Пион'))
    (flower_with_default_vals(colour='Розовый'))
    (flower_with_default_vals(price=15.5))
    (flower_with_default_vals(flower='Орхидея', colour='Синий'))
    (flower_with_default_vals(flower='Гиацинт', price=13.65))
    (flower_with_default_vals(colour='Желтый', price=11.8))
    (flower_with_default_vals(flower='Астра', colour='Фиолетовый', price=12.35))
