"""
lambda суммирует аргументы a, b и c и выводит результат
"""

abc = lambda a, b, c: a + b + c
print('abc: ', abc(11, 22, 33))

# ---------------------------------
#
# s = lambda x, y, z: print(x + y + z)
#
# if __name__ == '__main__':
#     s(44, 55, 66)
