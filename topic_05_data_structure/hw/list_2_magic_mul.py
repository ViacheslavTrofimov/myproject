"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def magic_mul(my_list: list):
    if type(my_list) != list:
        return 'Must be list!'
    if len(my_list) == 0:
        return 'Empty list!'

    end_list = []
    first_el = my_list[0]
    three_lists = my_list * 3
    last_el = my_list[-1]

    end_list.append(first_el)
    end_list.extend(three_lists)
    end_list.append(last_el)

    return end_list


if __name__ == '__main__':
    print(magic_mul([[1, 2, 3], 2, "my sister"]))
    # print(magic_mul([1]))


