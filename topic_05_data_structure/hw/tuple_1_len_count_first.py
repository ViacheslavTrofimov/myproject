"""
Функция len_count_first.

Принимает 2 аргумента: кортеж my_tuple и строку word.

Возвращает кортеж состоящий из
длины кортежа,
количества word в кортеже my_tuple,
первого элемента кортежа.

Пример:
my_tuple=('55', 'aa', '66')
word = '66'
результат (3, 1, '55').

Если вместо tuple передано что-то другое, то возвращать строку 'First arg must be tuple!'.
Если вместо строки передано что-то другое, то возвращать строку 'Second arg must be str!'.
Если tuple пуст, то возвращать строку 'Empty tuple!'.
"""


def len_count_first(my_tuple: tuple, word: str):
    if type(my_tuple) != tuple:
        return 'First arg must be tuple!'
    if type(word) != str:
        return 'Second arg must be str!'
    if len(my_tuple) == 0:
        return 'Empty tuple!'
    count_word = my_tuple.count(word)
    return len(my_tuple), count_word, my_tuple[0]


if __name__ == '__main__':
    print(len_count_first((1, '9', 5, 'Fight', [8, 9.0, None]), '9'))
