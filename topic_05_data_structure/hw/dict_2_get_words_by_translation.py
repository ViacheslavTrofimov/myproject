"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(my_dict: dict, word: str):
    if type(my_dict) != dict:
        return "Dictionary must be dict!"
    if type(word) != str:
        return "Word must be str!"
    if len(my_dict) == 0:
        return "Dictionary is empty!"
    if word == '':
        return "Word is empty!"
    else:
        meanings = []
        for lexeme, meaning in my_dict.items():
            if word in meaning:
                meanings.append(lexeme)

        return meanings if len(meanings) > 0 else f"Can't find English word: {word}"


if __name__ == '__main__':
    test_dict = {"человек": ["human", "man", "person"]}
    print(get_words_by_translation(test_dict, "man"))
