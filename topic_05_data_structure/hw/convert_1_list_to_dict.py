"""
Функция list_to_dict.

Принимает 2 аргумента: список и значение для поиска.

Возвращает словарь (из входного списка), в котором
ключ = значение из списка,
значение = (
индекс (последний, если есть несколько одинаковых значений) этого элемента в списке,
равно ли значение из списка искомому значению (True | False),
количество элементов в словаре.
)

Если список пуст, то возвращать пустой словарь.

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

ВНИМАНИЕ: количество элементов в словаре не всегда равно количеству элементов в списке!

ВНИМАНИЕ: при повторяющихся ключах в dict записывается значение последнего добавленного ключа.

ВНИМАНИЕ: нумерация индексов начинается с 0!
"""


def list_to_dict(my_list: list, val):
    if type(my_list) != list:
        return 'First arg must be list!'
    if len(my_list) == 0:
        return {}

    list_to_set = len(set(my_list))
    result_dict = dict()
    for index, elem in enumerate(my_list):
        result_dict[elem] = (index, elem == val, list_to_set)
    return result_dict


if __name__ == '__main__':
    print(list_to_dict(['a', 'bc', 'df', 'a'], 'a'))

# index = len(my_list) - my_list[::-1].index(val) - 1
