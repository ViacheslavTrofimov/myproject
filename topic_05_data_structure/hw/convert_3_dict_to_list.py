"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    if my_dict == {}:
        return [], [], 0, 0

    key_list = list(my_dict.keys())
    values_list = list(my_dict.values())
    unique_keys = len(my_dict.keys())
    unique_values = len(set(my_dict.values()))

    return key_list, values_list, unique_keys, unique_values


if __name__ == '__main__':
    print(dict_to_list({1: 'w', 2: 'w', 3: 'w'}))
