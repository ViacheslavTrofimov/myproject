"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""

from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker


class School:

    def __init__(self, people: list, number: str):
        self.people = people
        self.pupils = []
        self.stuff = []
        self.number = number

        self.people.extend([self.pupils, self.stuff])

    def get_avg_mark(self):
        all_marks = [x.get_avg_mark() for x in self.people if isinstance(x, Pupil)]
        sum_of_marks = sum(all_marks)
        return sum_of_marks / len(all_marks)

    def get_avg_salary(self):
        all_salaries = [x.salary for x in self.people if isinstance(x, Worker)]
        sum_salaries = sum(all_salaries)
        return sum_salaries / len(all_salaries)

    def get_pupil_count(self):
        return len([x for x in self.people if isinstance(x, Pupil)])

    def get_worker_count(self):
        return len([x for x in self.people if isinstance(x, Worker)])

    def get_pupil_names(self):
        return [x.name for x in self.people if isinstance(x, Pupil)]

    def get_unique_worker_positions(self):
        return set([x.position for x in self.people if isinstance(x, Worker)])

    def get_max_pupil_age(self):
        return max([x.age for x in self.people if isinstance(x, Pupil)])

    def get_min_worker_salary(self):
        return min([x.salary for x in self.people if isinstance(x, Worker)])

    def get_min_salary_worker_names(self):
        return [x.name for x in self.people if isinstance(x, Worker) if x.salary == self.get_min_worker_salary()]


if __name__ == '__main__':
    pupil1 = Pupil('Harry Potter', 11, {'Transfiguration': [4, 5, 4], 'Charms': [5, 5, 5],
                                        'Potions': [3, 3, 4], 'Defence against the Dark Arts': [5, 5, 4]})
    pupil2 = Pupil('Hermione Granger,', 11, {'Transfiguration': [5, 5, 5], 'Charms': [5, 5, 5],
                                             'Potions': [5, 4, 5], 'Defence against the Dark Arts': [5, 5, 5]})
    pupil3 = Pupil('Ronald Weasley', 11, {'Transfiguration': [4, 4, 4], 'Charms': [5, 4, 5],
                                          'Potions': [3, 3, 3], 'Defence against the Dark Arts': [4, 5, 4]})
    pupil4 = Pupil('Neville Longbottom', 11, {'Transfiguration': [3, 3, 4], 'Charms': [4, 3, 4],
                                              'Potions': [3, 3, 3], 'Defence against the Dark Arts': [3, 3, 3]})

    worker1 = Worker('Minerva McGonagall', 6000, 'Head of Gryffindor')
    worker2 = Worker('Filius Flitwick', 5000, 'Head of Ravenclaw')
    worker3 = Worker('Severus Snape', 6000, 'Head of Slytherin')
    worker4 = Worker('Pomona Sprout', 4500, 'Head of Hufflepuff')

    my_school = School([pupil1, pupil2, pupil3, pupil4, worker1, worker2, worker3, worker4], 'Hogwarts')

    print(School.get_avg_mark(my_school))
    print(School.get_avg_salary(my_school))
    print(School.get_worker_count(my_school))
    print(School.get_pupil_count(my_school))
    print(School.get_pupil_names(my_school))
    print(School.get_unique_worker_positions(my_school))
    print(School.get_min_worker_salary(my_school))
    print(School.get_max_pupil_age(my_school))
    print(School.get_min_salary_worker_names(my_school))
