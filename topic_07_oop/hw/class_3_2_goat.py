class Goat:
    """
    Класс Goat.

    Поля:
    имя: name,
    возраст: age,
    сколько молока дает в день: milk_per_day.

    Методы:
    get_sound: вернуть строку 'Бе-бе-бе',
    __invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
    __mul__: умножить milk_per_day на число. вернуть self
    """

    def __init__(self, name: str, age: int, milk_per_day: int):
        self.name = name
        self.age = age
        self.milk_per_day = milk_per_day

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        self.name = self.name[::-1].title()
        return self

    def __mul__(self, other):
        self.milk_per_day = self.milk_per_day * other
        return self


if __name__ == '__main__':
    goat = Goat('Джали', 5, 10)
    goat2 = Goat('Нюра', 3, 8)
    print(goat.get_sound())
    print(goat.__invert__())
    print(goat.__mul__(5))

    # Тут тоже не убирается!
