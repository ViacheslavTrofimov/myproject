"""
Класс Pupil.

Поля:
имя: name,
возраст: age,
dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

Методы:
get_all_marks: получить список всех оценок,
get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
get_avg_mark: получить средний балл (все предметы),
__le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
"""


class Pupil:

    def __init__(self, name: str, age: int, marks: dict):
        self.name = name
        self.age = age
        self.marks = marks

    def get_all_marks(self):
        list_of_all_marks = []
        for mark in self.marks.values():
            list_of_all_marks.extend(mark)
        return list_of_all_marks

    def get_avg_mark_by_subject(self, subject):
        if subject in self.marks.keys():
            average_mark_on_subject = self.marks.get(subject, ())
            return sum(average_mark_on_subject) / len(average_mark_on_subject)
        else:
            return 0

    def get_avg_mark(self):
        all_marks = []
        for mark in self.marks.values():
            all_marks.extend(mark)
        sum_of_all_marks = sum(all_marks)
        average_of_all_marks = sum_of_all_marks / len(all_marks)
        return average_of_all_marks

    def __le__(self, other):
        return self.get_avg_mark() <= other.get_avg_mark()


if __name__ == '__main__':
    pupil1 = Pupil('Harry Potter', 11, {'Transfiguration': [4, 5, 4], 'Charms': [5, 5, 5],
                                        'Potions': [3, 3, 4], 'Defence against the Dark Arts': [5, 5, 4]})
    pupil2 = Pupil('Hermione Granger,', 11, {'Transfiguration': [5, 5, 5], 'Charms': [5, 5, 5],
                                             'Potions': [5, 4, 5], 'Defence against the Dark Arts': [5, 5, 5]})
    print(pupil1.get_all_marks())
    print(pupil1.get_avg_mark_by_subject('Transfiguration'))
    print(pupil1.get_avg_mark())
