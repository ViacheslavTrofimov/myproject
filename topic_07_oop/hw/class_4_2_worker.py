"""
Класс Worker.

Поля:
имя: name,
зарплата: salary,
должность: position.

Методы:
__gt__: возвращает результат сравнения (>) зарплат работников.
__len__: возвращает количество букв в названии должности.
"""


class Worker:

    def __init__(self, name: str, salary: int, position: str):
        self.name = name
        self.salary = salary
        self.position = position

    def __gt__(self, other):
        return self.salary > other.salary

    def __len__(self):
        return len(self.position)


if __name__ == '__main__':
    worker1 = Worker('Minerva McGonagall', 6000, 'Head of Gryffindor')
    worker2 = Worker('Filius Flitwick', 5000, 'Head of Ravenclaw')
    worker3 = Worker('Severus Snape', 6000, 'Head of Slytherin')
    worker4 = Worker('Pomona Sprout', 4500, 'Head of Hufflepuff')
