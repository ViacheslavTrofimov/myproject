"""
    Класс Farm.

    Поля:
    животные (list из произвольного количества Goat и Chicken): zoo_animals
    (вначале список пуст, потом добавляем животных методами append и extend самостоятельно),
    наименование фермы: name,
    имя владельца фермы: owner.

    Методы:
    get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
    get_chicken_count: вернуть количество куриц на ферме,
    get_animals_count: вернуть количество животных на ферме,
    get_milk_count: вернуть сколько молока можно получить в день,
    get_eggs_count: вернуть сколько яиц можно получить в день.
"""

from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat


class Farm:

    def __init__(self, name: str, owner: str):
        self.zoo_animals = []
        self.name = name
        self.owner = owner

    def get_goat_count(self):
        count = 0
        for goat in self.zoo_animals:
            if isinstance(goat, Goat):
                count += 1
        return count

    def get_chicken_count(self):
        count = 0
        for chicken in self.zoo_animals:
            if isinstance(chicken, Chicken):
                count += 1
        return count

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        total_milk = 0
        for goat in self.zoo_animals:
            if type(goat) == Goat:
                total_milk += goat.milk_per_day
        return total_milk

    def get_eggs_count(self):
        total_eggs = 0
        for chicken in self.zoo_animals:
            if type(chicken) == Chicken:
                total_eggs += chicken.eggs_per_day
        return total_eggs


if __name__ == '__main__':
    farm = Farm('Ранчо Санчо', 'Доктор Айболит')
    farm.zoo_animals.extend([Chicken('Вита', 5, 1),
                             Chicken('Гита', 3, 0.5),
                             Chicken('Дита', 3, 0.3),
                             Goat('Джали', 5, 10),
                             Goat('Нюра', 3, 8)])

    print(farm.get_chicken_count())
    print(farm.get_goat_count())
    print(farm.get_animals_count())
    print(farm.get_eggs_count())
    print(farm.get_milk_count())