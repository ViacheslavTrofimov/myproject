class Chicken:
    """
    Класс Chicken.

    Поля:
    имя: name,
    номер загона: corral_num,
    сколько яиц в день: eggs_per_day.

    Методы:
    get_sound: вернуть строку 'Ко-ко-ко',
    get_info: вернуть строку вида:
    "Имя курицы: name
     Номер загона: corral_num
     Количество яиц в день: eggs_per_day"
    __lt__: вернуть результат сравнения количества яиц (<)
    """

    def __init__(self, name: str, corral_num: int, eggs_per_day: float):
        self.name = name
        self.corral_num = corral_num
        self.eggs_per_day = eggs_per_day

    def __lt__(self, other):
        return self.eggs_per_day < other.eggs_per_day

    def get_sound(self):
        return 'Ко-ко-ко'

    def get_info(self):
        return f"Имя курицы: {self.name}\nНомер загона: {self.corral_num}\nКоличество яиц в день: {self.eggs_per_day}"


if __name__ == '__main__':
    chicken = Chicken('Вита', 5, 1)
    chicken2 = Chicken('Гита', 3, 0.5)
    chicken3 = Chicken('Дита', 3, 0.3)

    # Никак желтое подчеркивание с get_sound не убирается!
