"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


# Сам, к сожалению, это сделать не смог. Зато полностью разобрался и понял, как надо, + воспроизвел по памяти
# оба варианта. На это всё ушло около 1,5 часов.

def count_odd_num(num):
    if type(num) != int:
        return "Must be int!"
    if num <= 0:
        return "Must be > 0!"

    # odd_count = 0
    # num_str = str(num)
    # for i in num_str:
    #     if int(i) % 2 == 1:
    #         odd_count += 1
    # return odd_count

    odd_count = 0
    while num > 0:      # 554781
        remainder = num % 10
        num //= 10
        if remainder % 2 == 1:
            odd_count += 1
    return odd_count

    # odd_count = 0
    # while num > 0:
    #     if num % 2 == 1:
    #         odd_count += 1
    #
    #     num //= 10
    # return odd_count


if __name__ == '__main__':
    print(count_odd_num(554781))
