"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(string):
    if type(string) == str:
        num_str = str(string)
        if string == "":
            print("Empty string!")
        elif len(num_str) > 5:
            print(num_str[0:3] + num_str[-3:len(num_str)])
        else:
            print(num_str[0] * len(num_str))


if __name__ == '__main__':
    print_symbols_if(123456789)
