"""
Функция print_nth_symbols.

Принимает строку и натуральное число n (целое число > 0).
Вывести символы с индексом n, n*2, n*3 и так далее.
Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'

Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
Если тип n не int, то вывести строку 'Must be int!'.

Если n больше длины строки, то вывести пустую строку.
"""


def print_nth_symbols(string, n):
    if type(n) == int:
        if n > 0:
            if n <= len(string):
                print(string[n::n])
            else:
                print("")
        else:
            print("Must be > 0!")
    else:
        print("Must be int!")


if __name__ == '__main__':
    print_nth_symbols("123456789", 1)

