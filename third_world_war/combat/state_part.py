from enum import Enum, auto


class StatePart(Enum):
    Ничего = auto()
    Правители = auto()
    Военные = auto()
    Гражданские = auto()

    @classmethod
    def min_value(cls):
        return cls.Ничего.value

    @classmethod
    def max_value(cls):
        return cls.Гражданские.value

    @classmethod
    def has_item(cls, name: str):
        return name in cls._member_names_
