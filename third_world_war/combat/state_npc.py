import random

from third_world_war.combat.nat_state import Natstate
from third_world_war.combat.participants import Participants
from third_world_war.combat.state_by_type import state_by_type
from third_world_war.combat.state_part import StatePart


class StateNPC(Natstate):
    def __init__(self):
        rand_type_value = random.randint(Participants.min_value(), Participants.max_value())
        rand_state_type = Participants(rand_type_value)

        rand_state_name = random.choice(list(state_by_type.get(rand_state_type, {}).keys()))

        super().__init__(rand_state_name, rand_state_type)

    def next_step_points(self, **kwargs):
        attack_point = StatePart(random.randint(StatePart.min_value(), StatePart.max_value()))
        defence_point = StatePart(random.randint(StatePart.min_value(), StatePart.max_value()))
        super().next_step_points(next_attack=attack_point,
                                 next_defence=defence_point)


if __name__ == '__main__':
    state_npc = StateNPC()
    state_npc.next_step_points()
    print(state_npc)
