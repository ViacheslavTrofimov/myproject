from third_world_war.combat.participants import Participants

state_by_type = {

    Participants.Россия: {'Владимир Путин': 'путин.jpg',
                          'Сергей Шойгу': 'шойгу.jpg'},


    Participants.США: {'Джо Байден': 'байден.jpg',
                       'Ллойд Остин': 'остин.jpg'},


    Participants.Китай: {'Си Цзиньпин': 'цзиньпин.jpg',
                        'Вэй Фэнхэ': 'фэнхэ.jpg'},


    Participants.Ватикан: {'Франциск': 'франциск.jpg',
                           'Гвардейцы': 'гвардейцы.jpg'}
}
