from third_world_war.combat.participants import Participants

state_defence_weaknesses_by_type = {

    Participants.Россия: (Participants.Китай,
                          Participants.Ватикан),

    Participants.США: (Participants.Россия,
                       Participants.Ватикан),

    Participants.Китай: (Participants.Ватикан,)
}
