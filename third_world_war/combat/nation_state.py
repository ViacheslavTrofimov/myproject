from enum import Enum, auto


class NationState(Enum):
    READY = auto()
    DEFEATED = auto()

