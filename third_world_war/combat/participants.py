from enum import Enum, auto


class Participants(Enum):
    """
    Information from https://pokemondb.net/type
    """
    Россия = auto()    # Россия
    США = auto()      # США
    Китай = auto()     # Китай
    Ватикан = auto()  # Ватикан

    @classmethod
    def min_value(cls):
        return cls.Россия.value   # США

    @classmethod
    def max_value(cls):
        return cls.Ватикан.value    # Ватикан
