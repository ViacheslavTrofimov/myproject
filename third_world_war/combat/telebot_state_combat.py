import json

import telebot
from telebot import types
from telebot.types import Message

from third_world_war.combat.state_part import StatePart
from third_world_war.combat.game_result import GameResult
from third_world_war.combat.nat_state import Natstate
from third_world_war.combat.state_npc import StateNPC
from third_world_war.combat.state_by_type import state_by_type
from third_world_war.combat.nation_state import NationState
from third_world_war.combat.participants import Participants

with open("./bot_token.txt", 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

state = {}

statistics = {}

stat_file = "game_stat.json"

body_part_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                               one_time_keyboard=True,
                                               row_width=len(StatePart))

body_part_keyboard.row(*[types.KeyboardButton(body_part.name) for body_part in StatePart])


def update_save_stat(chat_id, result: GameResult):
    print("Обновление статистики", end="...")
    global statistics

    chat_id = str(chat_id)

    if statistics.get(chat_id, None) is None:
        statistics[chat_id] = {}

    if result == GameResult.Победа:
        statistics[chat_id]['Победа'] = statistics[chat_id].get('Победа', 0) + 1
    elif result == GameResult.L:
        statistics[chat_id]['Поражение'] = statistics[chat_id].get('Поражение', 0) + 1
    elif result == GameResult.E:
        statistics[chat_id]['Взаимоуничтожение'] = statistics[chat_id].get('Взаимоуничтожение', 0) + 1
    else:
        print(f"Не существует результата {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("завершено!")


def load_stat():
    print('Загрузка статистики...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('завершена успешно!')
    except FileNotFoundError:
        statistics = {}
        print('файл не найден!')


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id,
                     "Hi!\n/start для начала игры\n/stat для отображения статистики")


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = "Еще не было ни одной игры"
    else:
        user_stat = "Твои результаты:"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,
                     text=user_stat)


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id,
                     text="Готов начать Третью Мировую?",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id,
                         'Хорошо, начинаем!')

        create_npc(message)

        ask_user_about_state_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id,
                         'Ок, я подожду.')
    else:
        bot.send_message(message.from_user.id,
                         'Я не знаю такого варианта ответа!')


def create_npc(message):
    print(f"Начало создания объекта NPC для chat id = {message.chat.id}")
    global state
    state_npc = StateNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_state'] = state_npc

    npc_image_filename = state_by_type[state_npc.participants][state_npc.name]
    bot.send_message(message.chat.id, 'Противник:')
    with open(f"../images/{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, state_npc)
    print(f"Завершено создание объекта NPC для chat id = {message.chat.id}")


def ask_user_about_state_type(message):
    markup = types.InlineKeyboardMarkup()

    for participants in Participants:
        markup.add(types.InlineKeyboardButton(text=participants.name,
                                              callback_data=f"participants_{participants.value}"))

    bot.send_message(message.chat.id, "Выбери государство:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "participants_" in call.data)
def participants_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 2 or not call_data_split[1].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        state_type_id = int(call_data_split[1])

        bot.send_message(call.message.chat.id, "Выбери свое государство:")

        ask_user_about_state_by_type(state_type_id, call.message)


def ask_user_about_state_by_type(state_type_id, message):
    participants = Participants(state_type_id)
    state_dict_by_type = state_by_type.get(participants, {})

    for state_name, state_img in state_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=state_name,
                                              callback_data=f"state_name_{state_type_id}_{state_name}"))
        with open(f"../images/{state_img}", 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "state_name_" in call.data)
def state_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        state_type_id, state_name = int(call_data_split[2]), call_data_split[3]

        create_user_state(call.message, state_type_id, state_name)

        bot.send_message(call.message.chat.id, "Игра началась!")

        game_next_step(call.message)


def create_user_state(message, state_type_id, state_name):
    print(f"Начало создания объекта Natstate для chat id = {message.chat.id}")
    global state
    user_state = Natstate(name=state_name,
                          participants=Participants(state_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_state'] = user_state

    image_filename = state_by_type[user_state.participants][user_state.name]
    bot.send_message(message.chat.id, 'Твой выбор:')
    with open(f"../images/{image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, user_state)

    print(f"Завершено создание объекта Natstate для chat id = {message.chat.id}")


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     "Выбор категории для защиты:",
                     reply_markup=body_part_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not StatePart.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Выбор категории для удара:",
                         reply_markup=body_part_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_body_part=message.text)


def reply_attack(message: Message, defend_body_part: str):
    if not StatePart.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        attack_body_part = message.text

        global state

        user_state = state[message.chat.id]['user_state']

        state_npc = state[message.chat.id]['npc_state']

        user_state.next_step_points(next_attack=StatePart[attack_body_part],
                                    next_defence=StatePart[defend_body_part])

        state_npc.next_step_points()

        game_step(message, user_state, state_npc)


def game_step(message: Message, user_state: Natstate, state_npc: Natstate):
    comment_npc = state_npc.get_hit(opponent_attack_point=user_state.attack_point,
                                    opponent_hit_power=user_state.hit_power,
                                    opponent_type=user_state.participants)
    bot.send_message(message.chat.id, f"NPC state: {comment_npc}\nЗащита: {state_npc.hp}")

    comment_user = user_state.get_hit(opponent_attack_point=state_npc.attack_point,
                                      opponent_hit_power=state_npc.hit_power,
                                      opponent_type=state_npc.participants)
    bot.send_message(message.chat.id, f"Your state: {comment_user}\nЗащита: {user_state.hp}")

    if state_npc.nat_state == NationState.READY and user_state.nat_state == NationState.READY:
        bot.send_message(message.chat.id, "Продолжаем!")
        game_next_step(message)
    elif state_npc.nat_state == NationState.DEFEATED and user_state.nat_state == NationState.DEFEATED:
        bot.send_message(message.chat.id, "Оба уничтожены!")
        update_save_stat(message.chat.id, GameResult.Взаимоуничтожение)
    elif state_npc.nat_state == NationState.DEFEATED:
        bot.send_message(message.chat.id, "Ты уничтожил!")
        update_save_stat(message.chat.id, GameResult.Победа)
    elif state_npc.nat_state == NationState.DEFEATED:
        bot.send_message(message.chat.id, "Ты уничтожен!")
        update_save_stat(message.chat.id, GameResult.Поражение)


if __name__ == '__main__':
    load_stat()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
