from third_world_war.combat.participants import Participants
from third_world_war.combat.states_weaknesses import state_defence_weaknesses_by_type as weaknesses_by_type
from third_world_war.combat.state_part import StatePart
from third_world_war.combat.nation_state import NationState


class Natstate:
    def __init__(self,
                 name: str,
                 participants: Participants):
        self.name = name
        self.participants = participants
        self.weaknesses = weaknesses_by_type.get(participants, tuple())
        self.hp = 100
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 20
        self.nat_state = NationState.READY

    def __str__(self):
        return f"Имя: {self.name} | Государство: {self.participants.name}\nЗащита: {self.hp}"

    def next_step_points(self,
                         next_attack: StatePart,
                         next_defence: StatePart):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_point: StatePart,
                opponent_hit_power: int,
                opponent_type: Participants):
        if opponent_attack_point == StatePart.Ничего:
            return "Честь имею"
        elif self.defence_point == opponent_attack_point:
            return "Насмешил мои Искандеры!"
        else:
            self.hp -= opponent_hit_power * (2 if opponent_type in self.weaknesses else 1)

            if self.hp <= 0:
                self.nat_state = NationState.DEFEATED
                return "Уничтожен"
            else:
                return "Поражен, но не уничтожен!"


if __name__ == '__main__':
    s = Natstate('Владимир Путин', Participants.Россия)
    print(s)
