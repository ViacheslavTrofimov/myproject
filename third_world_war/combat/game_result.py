from enum import Enum, auto


class GameResult(Enum):
    Победа = auto()
    Поражение = auto()
    Взаимоуничтожение = auto()

